# public_repo

This is a proof of concept where the code repo is empty but another private project is pushing published images into the container registry of this project

i need to share the deplou_token with write credentials (settings/repository/deploy_tokens)
in the token, specify a username GITUSER and the token value is GITSECRET

inside kaniko
`echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$GITUSER\",\"password\":\"$GITSECRET\"}}}" > /kaniko/.docker/config.json`
also i need to change the publish path from
`${CI_REGISTRY_IMAGE}/image:tag` to `registry.gitlab.com/xilng/public_repo/image:tag`